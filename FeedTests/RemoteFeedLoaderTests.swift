//
//  RemoteFeedLoaderTests.swift
//  FeedTests
//
//  Created by Diógenis Silva on 30/09/20.
//  Copyright © 2020 Diógenis Silva. All rights reserved.
//

import XCTest
import Feed

class RemoteFeedLoaderTests: XCTestCase
{
    //
    // MARK: - Tests
    
    func test_init_DoesNotRequestDataFromURL()
    {
        let (_, client) = makeSUT()
        
        XCTAssertTrue(client.requestedURLs.isEmpty)
    }
    
    func test_load_requestsDataFromURL()
    {
        let url = URL(string: "https://a-url.com")!
        let (sut, client) = makeSUT(url: url)
        
        sut.load(completion: { _ in })
        
        XCTAssertEqual(client.requestedURLs, [url])
    }
    
    func test_loadTwice_requestsDataFromURLTwice()
    {
        let url = URL(string: "https://a-url.com")!
        let (sut, client) = makeSUT(url: url)
        
        sut.load(completion: { _ in })
        sut.load(completion: { _ in })
        
        XCTAssertEqual(client.requestedURLs, [url, url])
    }
    
    func test_load_deliversErrorOnClientError()
    {
        let (sut, client) = makeSUT()
        
        var capturedErrors: [RemoteFeedLoader.Error] = []
        
        sut.load(completion: { (error) in
            capturedErrors.append(error)
        })
        
        let clientError = NSError(domain: "Test", code: 0)
        client.complete(with: clientError)
        
        XCTAssertEqual(capturedErrors, [.connectivity])
    }
    
    
    //
    // MARK: - Helper methods

    private func makeSUT(url: URL = URL(string: "https://a-given-url.com")!) -> (sut: RemoteFeedLoader, client: HTTPClientSpy)
    {
        let client = HTTPClientSpy()
        let sut = RemoteFeedLoader(url: url, client: client)
        
        return (sut, client)
    }
    
    
    //
    // MARK: - Helpers classes

    class HTTPClientSpy: HTTPClient
    {
        //
        // MARK: - Properties
        
        private var messages: [(url: URL, completion: (Error) -> Void)] = []
        
        var requestedURLs: [URL] {
            return messages.map({ $0.url })
        }
        
        
        //
        // MARK: - Public methods
        
        func get(from url: URL, completion: @escaping (Error) -> Void)
        {
            self.messages.append((url, completion))
        }
        
        func complete(with error: Error, at index: Int = 0)
        {
            self.messages[index].completion(error)
        }
    }
}
