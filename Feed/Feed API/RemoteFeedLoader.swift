//
//  RemoteFeedLoader.swift
//  Feed
//
//  Created by Diógenis Silva on 02/10/20.
//  Copyright © 2020 Diógenis Silva. All rights reserved.
//

import Foundation

public protocol HTTPClient
{
    func get(from url: URL, completion: @escaping (Error) -> Void)
}

public class RemoteFeedLoader
{
    public enum Error: Swift.Error
    {
        case connectivity
    }
    
    //
    // MARK: - Properties
    
    private let url: URL
    private let client: HTTPClient
    
    
    //
    // MARK: - Init methods
    
    public init(url: URL, client: HTTPClient)
    {
        self.url = url
        self.client = client
    }
    
    
    //
    // MARK: - Public methods
    
    public func load(completion: @escaping (Error) -> Void)
    {
        self.client.get(from: self.url, completion: { (error) in
            completion(.connectivity)
        })
    }
}
