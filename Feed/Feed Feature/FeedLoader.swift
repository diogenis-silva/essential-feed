//
//  FeedLoader.swift
//  Feed
//
//  Created by Diógenis Silva on 30/09/20.
//  Copyright © 2020 Diógenis Silva. All rights reserved.
//

import Foundation

enum LoadFeedResult
{
    case success([FeedItem])
    case error(Error)
}

protocol FeedLoader
{
    func load(completion: @escaping (LoadFeedResult) -> Void)
}
