//
//  FeedItem.swift
//  Feed
//
//  Created by Diógenis Silva on 30/09/20.
//  Copyright © 2020 Diógenis Silva. All rights reserved.
//

import Foundation

struct FeedItem
{
    let id: UUID
    let description: String?
    let location: String?
    let imageURL: URL
}
